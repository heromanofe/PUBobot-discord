#!/usr/bin/python2
# encoding: utf-8
import discord, traceback, time
from modules import console, config, bot, stats3

def init():
	global ready, send_queue
	ready = False
	send_queue = []

def process_connection():
	global ready

	console.display('SYSTEM| Logged in as: {0}, ID: {1}'.format(c.user.name, c.user.id))

	channels = stats3.get_channels()
	for cfg in channels:
		discord_channel = c.get_channel(cfg['channel_id'])
		if  discord_channel == None:
			console.display("SYSTEM| Could not find channel '{0}>{1}#' with CHANNELID '{2}'! Scipping...".format(cfg['server_name'], cfg['channel_name'], cfg['channel_id']))
			#todo: delete channel
		else:
			chan = bot.Channel(discord_channel, cfg)
			bot.channels.append(chan)
			console.display("SYSTEM| '{0}>{1}#' channel init successfull".format(chan.cfg['server_name'], chan.cfg['channel_name']))
	ready = True

def get_empty_servers():
	for serv in c.guilds:
		n=0
		for chan in serv.channels:
			if chan.id in [i.cfg['channel_id'] for i in bot.channels]:
				n=1
				break
		if not n:
			console.display("server name: {0}, id: {1}".format(serv.name, serv.id))

async def send(): #send messages in queue
	global send_queue
	if len(send_queue):
		for func, kwargs in send_queue:
			try:
				await func(**kwargs)
			except Exception as e:
				console.display("ERROR| could not send data ({0}). {1}".format(str(func), str(e)))
		send_queue = []

async def close(): #on quit
	if c.is_closed():
		try:
			await c.logout()
			print("Successfully logged out.")
		except Exception as e:
			print("Error on logging out. {0}".format(str(e)))
	else:
		print("Connection is allready closed.")

### api for bot.py ###
def find_role_by_name(channel, name):
	name = name.lower()
	server = c.get_guild(channel.guild.id)
	if server:
		for role in server.roles:
			if name == role.name.lower():
				return role
	return None

def find_role_by_id(channel, role_id):
	server = c.get_guild(channel.guild.id)
	if server:
		for role in server.roles:
			if role_id == role.id:
				return role
	return None

async def edit_role(role, **fields):
	await role.edit(**fields)


async def Timer_started(seconds,mess):
	message = await message.channel.send(mess)
	timer = seconds/2
	and_sec = str(seconds)[len(str(seconds))-1]
	and_sec = int(and_sec)
	temp = ""
	for x in range(len(str(seconds))-1):
		temp = str(seconds)[x]
	seconds = int(temp)
	mm = message.content
	while timer >= 0:
		and_sec -= 2
		if and_sec <= -1:
			seconds -= 1
			and_sec = 9
		await asyncio.sleep(2)
		mm = "```md\n#Timer started! ---> |"+numbers(seconds)+""+numbers(and_sec)+"| Seconds left\n```"
		timer -= 1
		await message.edit(content=str(mm))
	await message.delete()

async def Voting(mentionss,mmm,mda,seconds,Maps):
		embed = discord.Embed(colour=discord.Colour(0x50e3c2), description="\|{0}".format(mmm))
		for i in mda:
				embed.add_field(name="**MAP LIST {0}**".format(count+1), value="{0}".format(i), inline=True)
				count += 1
		message = await channel.send(content=str("**                                  VOTE For A Map**"), embed=embed)

		maps = []
		class Map:
			def __init__(self, name, emoji):
				self.name = name
				self.emoji = emoji
		Maps = []
		emojis_check = []
		mm = "\n"
		last_map = "map, map".split(',')
		for i in Maps:
			if i.name not in Maps:
				await message.add_reaction(i.emoji)
				maps.append(i.name)
				if i.emoji[0] == "<":
					reg = re.compile(r"\d{18}")
					r = re.search(reg, i.emoji)
					r = int(r.group(0))
					emojis_check.append(client.get_emoji(r))
				else:
					emojis_check.append(i.emoji)
		await asyncio.sleep(seconds+1) #####################################
		max = 0
		maxxx=[]
		count = 0
		last_max = 0
		kk=0
		mm = mm + "People,who can vote are: "
		for i in mentionss:
			mm=mm+"|"+i.display_name+"|"
		mm = mm + "\n----------------------------------------------\n"
		for n in message.reactions:
			for o in emojis_check:
				if o == n.emoji:
					users = await n.users().flatten()
					new_count = 0
					for user in users:
						for i in mentionss:
							if user.id==i.id:
								new_count +=1
								mm = mm + "|"+user.display_name+"| "
					if new_count > last_max:
						max = count
						last_max = new_count
					if new_count != 0:
						kk+=1
						new = "<"
						o = str(o)
						mm = mm + "voted for: "+o+"\t"+ "In total: ["+str(new_count)+"] votes for "+o+"which is: **"+maps[count]+"**\n"
					count+=1
		count = 0
		for n in message.reactions:
			for o in emojis_check:
				if o == n.emoji:
					users = await n.users().flatten()
					new_count = 0
					for user in users:
						for i in mentionss:
							if user.id==i.id:
								new_count +=1
					if new_count == last_max:
						maxxx.append(count)
					count+=1      
		if len(maxxx)>1 or kk == 0:
			max = maxxx[random.randrange(-1, len(maxxx))]
		if len(last_map) < 2:
			last_map.append(maps[max])
		else: last_map[len(last_map)-1]=maps[max]
		mm = mm + "\n `Map is: {0}`\n".format(maps[max])
		await message.channel.send("Map is: **{0}**".format(maps[max]))
		last = ""
		for i in last_map:
			last+=i+","
		c.execute('UPDATE Guilds SET and_another_map = ? where Guild_id= ? and channel_id==?', (and_another_map, message.guild.id,message.channel.id))
		self.last_map = last_map
		c.close()
		conn.commit()
		conn.close()
		embed = discord.Embed(colour=discord.Colour(0x856fef))
		number13 = 0
		for i in mentionss:
			if number13<players_per_team:
				alpha_players+="{0}\n".format(i.display_name)
			else: bravo_players +="{0}\n".format(i.display_name)
			number13+=1
		wh = ""
		wh2 = ""
		if side == 1:
			wh = "glsl"
			wh2 = "md"
		else: 
			wh="md"
			wh2="glsl"
		embed.add_field(name="`    [`{0}`] Alpha [`{0}`]   |`".format(alpha_side), value="```{2}\n#{0:^20}``````fix\n{1}```".format(alpha_name,alpha_players,wh), inline=True)
		embed.add_field(name="`   [`{0}`] Bravo [`{0}`]   `".format(bravo_side), value="```{2}\n#{0:^18}``````fix\n{1}```".format(bravo_name,bravo_players,wh2), inline=True)
		embed.add_field(name="\u200B", value="```fix\n{0:^38}```".format("Map: "+maps[max]), inline=False)
		await message.channel.send(embed = embed)
		await message.delete()

async def remove_roles(member, *roles):
	await member.remove_roles(*roles)

async def add_roles(member, *roles):
	await member.add_roles(*roles)

def notice(channel, msg):
	console.display("SEND| {0}> {1}".format(channel.name, msg))
	send_queue.append([channel.send, {'content': msg}])

def reply(channel, member, msg):
	console.display("SEND| {0}> {1}, {2}".format(channel.name, member.nick or member.name, msg))
	send_queue.append([channel.send, {'content': "<@{0}>, {1}".format(member.id, msg)}])
	
def private_reply(member, msg):
	console.display("SEND_PM| {0}> {1}".format(member.name, msg))
	send_queue.append([member.send, {'content': msg}])

def delete_message(msg):
	send_queue.append([msg.delete, {}])

def edit_message(msg, new_content):
	console.display("EDIT| {0}> {1}".format(msg.channel.name, new_content))
	send_queue.append([msg.edit, {'content': new_content}])

def add_reaction(msg, emoji):
	send_queue.append([msg.add_reaction, {'emoji': emoji}])
	
def get_member_by_nick(channel, nick):
	server = c.get_guild(channel.guild.id)
	return discord.utils.find(lambda m: m.name == nick, server.members)

def get_member_by_id(channel, highlight):
	memberid = highlight.lstrip('<@!').rstrip('>')
	if memberid.isdigit():
		memberid = int(memberid)
		server = c.get_guild(channel.guild.id)
		return discord.utils.find(lambda m: m.id == memberid, server.members)
	else:
		return None

### discord events ###
c = discord.Client()

@c.event
async def on_ready():
	global ready
	if not ready:
		process_connection()
		ready = True
	else:
		console.display("DEBUG| Unexpected on_ready event!")
	await c.change_presence(activity=discord.Game(name='pm !help'))

@c.event
async def on_message(message):
#	if message.author.bot:
#		return
	if isinstance(message.channel, discord.abc.PrivateChannel) and message.author.id != c.user.id:
		console.display("PRIVATE| {0}>{1}>{2}: {3}".format(message.guild, message.channel, message.author.display_name, message.content))
		private_reply(message.author, config.cfg.HELPINFO)
	elif message.content == '!enable_pickups':
		if message.channel.permissions_for(message.author).manage_channels:
			if message.channel.id not in [x.id for x in bot.channels]:
				newcfg = stats3.new_channel(message.guild.id, message.guild.name, message.channel.id, message.channel.name, message.author.id)
				bot.channels.append(bot.Channel(message.channel, newcfg))
				reply(message.channel, message.author, config.cfg.FIRST_INIT_MESSAGE)
			else:
				reply(message.channel, message.author, "this channel allready have pickups configured!")
		else:
			reply(message.channel, message.author, "You must have permission to manage channels to enable pickups.")
	elif message.content == '!disable_pickups':
		if message.channel.permissions_for(message.author).manage_channels:
			for chan in bot.channels:
				if chan.id == message.channel.id:
					bot.delete_channel(chan)
					reply(message.channel, message.author, "pickups on this channel have been disabled.")
					return
			reply(message.channel, message.author, "pickups on this channel has not been set up yet!") 
		else:
			reply(message.channel, message.author, "You must have permission to manage channels to disable pickups.")
	elif message.content != '':
		for channel in bot.channels:
			if message.channel.id == channel.id:
				try:
					await channel.processmsg(message)
				except:
					console.display("ERROR| Error processing message: {0}".format(traceback.format_exc()))

@c.event
async def on_member_update(before, after):
	#console.display("DEBUG| {0} changed status from {1}  to -{2}-".format(after.name, before.status, after.status))
	if str(after.status) in ['idle', 'offline']:
		bot.update_member(after)

@c.event
async def on_member_remove(member):
	bot.member_left(member)

@c.event
async def on_reaction_add(reaction, user):
	if reaction.message.id in bot.waiting_reactions.keys():
		bot.waiting_reactions[reaction.message.id]('add', reaction, user)

@c.event
async def on_reaction_remove(reaction, user):
	if reaction.message.id in bot.waiting_reactions.keys():
		bot.waiting_reactions[reaction.message.id]('remove', reaction, user)

### connect to discord ###
def run():
	while True:
		try:
			if config.cfg.DISCORD_TOKEN != "":
				console.display("SYSTEM| logging in with token...")
				c.loop.run_until_complete(c.start(config.cfg.DISCORD_TOKEN))
			else:
				console.display("SYSTEM| logging in with username and password...")
				c.loop.run_until_complete(c.start(config.cfg.USERNAME, config.cfg.PASSWORD))
			c.loop.run_until_complete(c.connect())
		except KeyboardInterrupt:
			console.display("ERROR| Keyboard interrupt.")
			console.terminate()
			c.loop.run_until_complete(close())
			print("QUIT NOW.")
			break
		except Exception as e:
			console.display("ERROR| Disconnected from the server: "+str(e)+"\nReconnecting in 15 seconds...")
			time.sleep(15)
